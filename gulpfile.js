//list dependences
const {src, dest, watch, series} = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const prefix = require('gulp-autoprefixer');
const minify = require('gulp-clean-css');
const gulp = require("gulp");


gulp.task('styles', function (done){
    return src('src/scss/*.scss')
        .pipe(sass())
        .pipe(prefix())
        .pipe(minify())
        .pipe(dest('src/css'))
})


function styles(){
    return src('src/scss/*.scss')
        .pipe(sass())
        .pipe(prefix())
        .pipe(minify())
        .pipe(dest('/dist/css'))
}

function watchTask(){
    watch('src/scss/*.scss'. styles);
}

exports.default = series(
    styles,
)


