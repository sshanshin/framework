<?php
error_reporting(-1);

use vendor\core\Router;
include 'config/include.php';

$query = ltrim($_SERVER['REQUEST_URI'],'/');

//defaults roots
Router::add('main/(?P<action>[a-z-]+)/(?P<alias>[a-z-]+)$', ['controller' => 'Main']);
Router::add('^$', ['controller' => 'Main', 'action' => 'index']);
Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$' );

Router::dispatch($query);
