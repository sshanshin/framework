<?php
include 'vendor/libs/functions.php';
include 'smarty/libs/Smarty.class.php';
include 'config/define.php';

spl_autoload_register(function ($class){
    $file = ROOT.'/'.str_replace('\\','/',$class).'.php';
    if(is_file($file)){
        require_once $file;
    }
});