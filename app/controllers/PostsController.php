<?php

namespace app\controllers;

use app\models\Main;
use app\models\Posts;

class PostsController extends AppController {
	public function indexAction() {
		$model = new Posts();
		$model->display();
	}
}