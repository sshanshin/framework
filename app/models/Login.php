<?php

namespace app\models;

use vendor\core\base\Model;

class Login extends Model {
    public function __construct() {
        parent::__construct();
        $this->tpl = 'login.tpl';
    }
}