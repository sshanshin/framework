<?php

namespace vendor\core;

class Db {
	protected $pdo;
	protected static $instance;
	private static $totalTime = 0;
	private static $debug = 'sql-debug';

	protected function __construct() {
		$db = require ROOT.'/config/config_db.php';
		$options = [
			\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
			\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
		];
		$this->pdo = new \PDO($db['dsn'], $db['user'], $db['pass'], $options);
	}

	public static function instance() {
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function execute($sql, $params = []) {
		$stmt = $this->pdo->prepare($sql);
		return $stmt->execute($params);
	}

	public function query($sql, $params = []) {
		$exec = microtime(true);;

		$stmt = $this->pdo->prepare($sql);
		$res =  $stmt->execute($params);
		if ($res) {
			$done = $stmt->fetchAll();
			$exec = (microtime(true) - $exec);
			self::$totalTime+= $exec;
			$this->log($sql, $exec, self::$totalTime);
			return $done;
		}
		return [];
	}

	private function log($sql, $exec, $total) {
		if ($_SERVER['QUERY_STRING'] == self::$debug) {
			$text = "\n**************************************************\n";
			$exec = 'Exec time - ' . $exec;
			$total = 'Total time - ' . $total;
			$text .= "$sql\n$exec\n$total\n";
			file_put_contents('mysql.log', $text, FILE_APPEND);
		}
	}
}