<?php

namespace vendor\core;

class Router{

    protected static $routes = array();
    protected static $route = array();

    public static function add($regexp,$route = []) {
        self::$routes[$regexp] = $route;
    }

    public static function getRoutes() {
        return self::$routes;
    }

    public static function getRoute() {
        return self::$route;
    }

    protected static function matchRoute($url) {
        foreach (self::$routes as $pattern => $route) {
            if (preg_match("#$pattern#i", $url, $matches)) {
                foreach ($matches as $key => $value){
                    if (is_string($key)) {
                        $route[$key] = $value;
                    }
                }
                if (!isset($route['action'])) {
					$route['action'] = 'index';
                }
				$route['controller'] = self::upperCamelCase($route['controller']);
                self::$route = $route;
                return true;
            }
        }
        return false;
    }


    /**
     * перенапрявляет URL по корректному маршруту
     * @param string $url входящий URL
     * @return void
     */
    public static function dispatch($url) {
		$url = self::removeQueryString($url);
        if (self::matchRoute($url)) {
            $controller = 'app\controllers\\' . self::upperCamelCase(self::$route['controller']).'Controller';
            if (class_exists($controller)) {
                $cObj = new $controller(self::$route);
                $action = self::lowerCamelCase(self::$route['action']) . 'Action';
                if (method_exists($cObj, $action)) {
                    $cObj->$action();
					$cObj->getView();
                } else {
                    echo "Метод <b>$action</b> у <b>$controller</b> не найден";
                }
            } else {
                echo "Контроллер <b>$controller</b> не найден";
            }
        } else {
            http_response_code(404);
            include "public/404.html";
        }
    }


	protected static function upperCamelCase($name){
		return str_replace(' ','',ucwords(str_replace('-',' ',$name)));
	}

	protected static function lowerCamelCase($name){
		return lcfirst(str_replace(' ','',ucwords(str_replace('-',' ',$name))));
	}

	protected static function removeQueryString($url){
		return $url ? explode('?', $url)[0] : $url;
	}
}