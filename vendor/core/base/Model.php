<?php

namespace vendor\core\base;

use Smarty;
use vendor\core\Db;

abstract class Model {

	protected $pdo;
	protected $table;
	protected $tpl;
	protected $smarty;

	public function __construct() {
		$this->pdo = Db::instance();
		$this->smarty = new Smarty();
	}

	public function query($sql) {
		return $this->pdo->query($sql);
	}

	public function findAll() {
		$sql = 'SELECT * FROM '.$this->table;
		return $this->pdo->query($sql);
	}

	public function display() {
		$this->smarty->display($this->tpl);
	}
}