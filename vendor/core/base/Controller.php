<?php

namespace vendor\core\base;

abstract class Controller {

	/**
	 * Пользовательсике данные
	 * @var array
	 */
	public $vars = [];

	public $route = array();
	public $view;
	public $layout;

	public function __construct($construct) {
		$this->route = $construct;
		$this->view = $this->route['action'];
	}

	public function getView() {
		$vObj = new View($this->route, $this->layout, $this->view);
		$vObj->render($this->vars);
	}

	public function set($vars) {
		$this->vars = $vars;
	}
}